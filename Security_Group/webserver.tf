resource "aws_instance" "webserver" {
  ami                    = "ami-0b0dcb5067f052a63"
  instance_type          = "t2.micro"
  vpc_security_group_ids = aws_security_group.webserver_sg.id
  key_name               = "terraform"
  tags = {
    Name = "My-EC2-instance"
  }
}
